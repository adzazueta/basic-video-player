const $video = document.querySelector("#video");
const $progressBar = document.querySelector("#progress-bar");
const $play = document.querySelector("#play");
const $pause = document.querySelector("#pause");
const $backward = document.querySelector("#backward");
const $forward = document.querySelector("#forward");

function handleMetaData() {
  $progressBar.max = $video.duration;
}

function handleTimeUpdate() {
  $progressBar.value = $video.currentTime;
}

function handleChangeTime() {
  $video.currentTime = $progressBar.value;
}

function handlePlay() {
  $video.play();
  $play.hidden = true;
  $pause.hidden = false;
}

function handlePause() {
  $video.pause();
  $pause.hidden = true;
  $play.hidden = false;
}

function handleBackward() {
  $video.currentTime -= 10;
}

function handleForward() {
  $video.currentTime += 10;
}

$video.addEventListener("loadedmetadata", handleMetaData);
$video.addEventListener("timeupdate", handleTimeUpdate);
$progressBar.addEventListener("input", handleChangeTime);
$play.addEventListener("click", handlePlay);
$pause.addEventListener("click", handlePause);
$backward.addEventListener("click", handleBackward);
$forward.addEventListener("click", handleForward);
